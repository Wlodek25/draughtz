/// @file app.js
/// @brief main client module, AngularJS application with routing

angular.module('myApp', ['ngRoute', 'pascalprecht.translate', 'myAppControllers', 'myAppServices'] )
    .config(['$routeProvider', '$translateProvider',
             function($routeProvider, $translateProvider) {
                 $routeProvider.when('/about', {
                     templateUrl: 'views/about.html',
                 });
				 $routeProvider.when('/play', {
                     templateUrl: 'views/draughtz.html',
                 });
				 $routeProvider.when('/login', {
                     templateUrl: 'views/login.html',
                 });
				 $routeProvider.when('/register', {
                     templateUrl: 'views/register.html',
                 });
				 $routeProvider.when('/score', {
                     templateUrl: 'views/score.html',
                 });
				 $routeProvider.when('/main', {
                     templateUrl: 'views/main.html',
                 });
                 $routeProvider.otherwise( {
                     redirectTo: '/login'
                 });
                 $translateProvider.useStaticFilesLoader({
                      prefix: 'lang/',
                      suffix: '.json' });
                 $translateProvider.preferredLanguage('en');
             }]);

