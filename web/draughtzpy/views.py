## @file calcpy/views.py
#  @brief calculation library interface to client

"""
calc library interface to client

export calculation results to client
"""
import draughtz

def getNumber(params):
    """the game logic from C++ library"""
    return {
        "number" : draughtz.getNumber()
    }
