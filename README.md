*Kordaczuk Bartosz,
Pilarczyk Rafał,
Włodkowski Patryk, ZPR 15L*

# Zależności #

**C++**

* obliczanie dostępnych dla użytkownika pól

* obliczanie możliwych do wykonania ruchów

* współgranie numeryczne z Pythonem

* Potraktowany jako układ WE/WY

* Niezależny od całej reszty architektury

**Python:**

* Odczytuje informacje z bazy danych PostgreSQL

* Wysyła odpowiedzi JSON, udostępnia pliki

* Obsługuje sesję użytkownika

* Odbiera wywołania funkcji napisanych w C++ inicjowanych przez klienta (AngularJS) i dzięki Boost::Python wykonuje kod napisany w C++. Następnie otrzymany wynik przesyła jak JSON do klienta.

**AngularJS**

* odpytuje serwer wykorzystując AJAX

* otrzymuje informacje pochodzące z serwera dotyczące użytkownika, ruchów (JSON)

* Odpowiada za wizualizację, widoki, oddzielenie części serwerowej od klienckiej wykorzystując

* kontrolery

* routing

* modele

* odpowiednio zmienia ostylowanie CSS

**Bootstrap**

* Framework do łatwiejszego ostylowania strony

**PostgreSQL**

* zawiera tabele z informacjami o danym użytkowniku: hasłem, loginem, liczbą punktów, wygranych/przegranych potyczkach, dacie ostatniego zalogowania etc.


#Budowanie#

* instalacja według instrukcji http://bioweb.sourceforge.net/en/index.html (mogą być problemy po stronie windows - większość zależności można rozwiazać rozwiazując problem PATH)

* Należy zwrócić uwagę, aby Python i Boost były w takiej samej architekturze! 

* W przypadku problemów *module does not define init function* problem dotyczy braku widoczności bibliotek boost::python

* scons

* scons syncdb=1

* scons r=l


* Budowanie BOOST 64 bit

```
#!cmd


run in a 64 bit terminal window of Visual Studio the commands 'bootstrap.bat', then

b2 -a variant=debug,release link=shared,static threading=multi address-model=64 toolset=msvc-12.0 stage
b2 -a variant=debug,release link=shared,static threading=multi address-model=64 toolset=msvc-12.0 stage

copy stage/lib/* to C:\Boost\lib
copy boost/* to C:\Boost\include\boost-1_57\boost
Add C:\Boost\lib to PATH
```

* W przypadku przeglądarki (m.in. Firefox) może wystąpić problem z Cache - AngularJS! Należy wówczas użyć kombinacji klawiszy * ctrl+F5 *

# Co zostało zrobione #

* Każda osoba z zespołu ma skonfigurowane repozytorium

* Zespół zna architekturę frameworku, członkowie wiedzą jak działają poszczególne elementy

* Każdy w zespole ma zainstalowane odpowiednie narzędzia wykorzystywane w projekcie

* Zostały napisane wstępne szablony stron, dokonano podstawowego routingu szablonów w AngularJS

* Zespół ma zbudowane biblioteki boost, projekt się buduje na każdym komputerze (Linux/Windows)

* Zostały postawione podstawowe cele projektowe i rozwiązane zależności pomiędzy poszczególnymi modułami projektowymi


# Skrócona funkcjonalność aplikacji #

**Strona główna serwisu**

* logowanie

* rejestracja 

**Po zalogowaniu otrzymujemy menu**

* Pokaż dostępnych graczy

* Kliknięcie na gracza powoduje wyzwanie go na pojedynek

* inny gracz otrzymuje komunikat - może przyjac wyzwanie/odrzucić

* w przypadku odrzucena -> drugi gracz otrzymuje informacje o odrzuceniu

* Po 30 sekundach wyzwanie jest automatycznie odrzucane

* Pokaż ranking na portalu - Pobierana z bazy danych Postgre

* Pokaż swoje statystyki - Fragment pobierany z bazy danych Postgre

* Wyloguj się

**Plansza gry :**

* biało czarna 10x10

* czerwone/białe pionki

* zegar odmierzający czas od rozpoczęcia rozgrywki

* menu z przyciskiem "wyjście z gry"

* informacja o tym do kogo należy tura

* informacja o przegranej 

**Poruszanie się:**

* Po kliknięciu na pionka

* otrzymujemy informacje, gdzie się możemy poruszyć poprzez odpowiednie ostylowanie CSS

* Klikamy tam gdzie się chcemy poruszyć

* W przypadku wielokrotne zbicia, tura się nie kończy, użytkownik może się 
poruszyć raz jeszcze

* Po zbiciu pionka automatycznie jest on usuwany