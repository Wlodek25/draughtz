/**
 * \file draugtz.hpp
 * \brief the C++ calculation library interface
 */

#ifndef DRAUGHTZ_HPP
#define DRAUGHTZ_HPP

#ifdef DRAUGHTZ_EXPORTS
/** Workaround for Windows DLL library exports */
#define DRAUGHTZ_DLL(X) __declspec(dllexport)X
#else
/** Workaround for Unix Shared Library exports */
#define DRAUGHTZ_DLL(X) X
#endif

//! Example C++ calculation. This function return a number.
DRAUGHTZ_DLL( int getNumber(); )

#endif //DRAUGHTZ_HPP
